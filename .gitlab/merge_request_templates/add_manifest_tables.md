Closes

* List the tables added/changed below
* Run the `clone_raw` CI job
* Run the `pgp_test` CI job. Include the `MANIFEST_NAME` variable and input the name of the db (i.e. `gitlab_com`)
  * Optionally include `advanced_metadata: true` if this is a SCD table

#### Tables Changed/Added

* [ ] List

#### PGP Test CI job passed?

* [ ] List
